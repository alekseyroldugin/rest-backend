﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RESTBackend.Models;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace RESTBackend.Controllers
{
    public class QueriesController : ApiController
    {
        private QueryContext db = new QueryContext();

        // GET: getqueries
        public IQueryable<Query> GetQueries()
        {
            return db.Queries;
        }

        // GET: getqueries
        public IQueryable<CallNoIdsDTO> GetCalls()
        {
            var calls = from b in db.Calls
                        select new CallNoIdsDTO()
                        {
                            Timestamp = b.Timestamp,
                            Ip = b.Ip,
                            Url = b.Url
                        };
            return calls;
        }

        // GET: getqueries/{id}
        [ResponseType(typeof(Query))]
        public async Task<IHttpActionResult> GetQuery(int id)
        {
            Query query = await db.Queries.FindAsync(id);
            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: editquery/{id}
        [ResponseType(typeof(Query))]
        public async Task<IHttpActionResult> PutQuery(int id, [FromBody]Query query)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != query.Id)
            {
                return BadRequest();
            }

            List<int> codes = GetHttpCodes();
            if (!codes.Contains(query.HttpCode))
            {
                return new StatusCodeResult(HttpStatusCode.Forbidden, Request);
            }

            db.Entry(query).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QueryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(query);
        }

        // POST: define-api-call
        public async Task<HttpResponseMessage> DefineApiCall([FromBody]QueryNoIdDTO json) // Изменил автоматически созданный метод, 
        // чтобы он принимал json
        {
            if (!ModelState.IsValid)
            {
                HttpResponseMessage badrequest = Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                return badrequest;
            }
            List<int> codes = GetHttpCodes();
            if (!codes.Contains(json.HttpCode))
            {
                HttpResponseMessage forbidden = Request.CreateResponse(HttpStatusCode.Forbidden, "Please, enter valid HttpCode");
                return forbidden;
            }
            Query query = new Query();
            query.Id = db.Queries.Count() + 1;
            query.Url = json.Url;
            query.Body = json.Body;
            query.HttpCode = json.HttpCode;
            db.Queries.Add(query);
            await db.SaveChangesAsync();

            HttpResponseMessage okey = Request.CreateResponse(HttpStatusCode.OK, "");
            return okey;
        }

        // POST: test-api-url
        public async Task<HttpResponseMessage> TestUrl(string url)
        {
            Query query = await db.Queries.FirstOrDefaultAsync(m => m.Url == url);
            if (query == null)
            {
                HttpResponseMessage notfound = Request.CreateResponse(HttpStatusCode.NotFound);
                return notfound;
            }

            Call call = new Call();
            call.Id = db.Calls.Count() + 1;
            call.QueryId = query.Id;
            call.Timestamp = HttpContext.Current.Timestamp;
            call.Ip = GetVisitorIPAddress();
            call.Url = query.Url;
            db.Calls.Add(call);
            await db.SaveChangesAsync();

            HttpResponseMessage response = Request.CreateResponse((HttpStatusCode)query.HttpCode, query.Body);
            return response;
        }

        // POST: list
        public async Task<HttpResponseMessage> List([FromBody]QueryUrlOnlyDTO json) // Реализация метода list
        {
            Query query = await db.Queries.FirstOrDefaultAsync(m => m.Url == json.Url);
            if (query == null)
            {
                HttpResponseMessage notfound = Request.CreateResponse(HttpStatusCode.NotFound);
                return notfound;
            }

            List<Call> calls = new List<Call>();
            foreach (Call counter in db.Calls.Where(m => m.QueryId == query.Id))
            {
                calls.Add(counter);
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, calls);
            return response;
        }

       

        public static List<int> GetHttpCodes()
        {
            List<int> listOfHttpCodes = new List<int>();
            int[] codes = new int[] { 226, 426, 428, 429, 431, 440, 598, 599};

            listOfHttpCodes.AddRange(codes);
            listOfHttpCodes.AddRange(Enumerable.Range(100, 3));
            listOfHttpCodes.AddRange(Enumerable.Range(200, 9));
            listOfHttpCodes.AddRange(Enumerable.Range(300, 9));
            listOfHttpCodes.AddRange(Enumerable.Range(400, 25));
            listOfHttpCodes.AddRange(Enumerable.Range(449, 3));
            listOfHttpCodes.AddRange(Enumerable.Range(500, 12));

            return listOfHttpCodes;
        }
        
        public static string GetVisitorIPAddress(bool GetLan = false)
        {
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan && string.IsNullOrEmpty(visitorIPAddress))
            {
                //This is for Local(LAN) Connected ID Address
                string stringHostName = Dns.GetHostName();
                //Get Ip Host Entry
                IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                //Get Ip Address From The Ip Host Entry Address List
                IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                try
                {
                    visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                }
                catch
                {
                    try
                    {
                        visitorIPAddress = arrIpAddress[0].ToString();
                    }
                    catch
                    {
                        try
                        {
                            arrIpAddress = Dns.GetHostAddresses(stringHostName);
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            visitorIPAddress = "127.0.0.1";
                        }
                    }
                }
            }
            return visitorIPAddress;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QueryExists(int id)
        {
            return db.Queries.Count(e => e.Id == id) > 0;
        }
    }
}