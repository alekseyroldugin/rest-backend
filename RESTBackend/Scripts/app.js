﻿var ViewModel = function () {
    var self = this;
    self.queries = ko.observableArray();
    self.error = ko.observable();
    self.selectedQuery = ko.observable();
    self.statistics = ko.observableArray();
    self.moreStatistics = ko.observableArray();
    self.testUrl = ko.observable();

    self.editable = {
        Id: ko.observable(),
        Url: ko.observable(),
        Body: ko.observable(),
        HttpCode: ko.observable()
    }

    self.newQuery = {
        Url: ko.observable(),
        Body: ko.observable(),
        HttpCode: ko.observable()
    }

    var queriesUri = '/getqueries/';
    var queryUri = '/editquery/';
    var defineApiCallUri = '/define-api-call/';
    var callsUri = '/getcalls/';
    var listUri = '/list/';

    function ajaxHelper(uri, method, data) {
        self.error('');
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    function getAllQueries() {
        ajaxHelper(queriesUri, 'GET').done(function (data) {
            self.queries(data);
        });
    }

    function getAllCalls() {
        ajaxHelper(callsUri, 'GET').done(function (data) {
            self.statistics(data);
        });
    }

    function paste(item) {
        self.editable.Id(item.Id);
        self.editable.Url(item.Url);
        self.editable.Body(item.Body);
        self.editable.HttpCode(item.HttpCode);
    };

    self.getQuery = function (item) {
        paste(item);
        ajaxHelper(queriesUri + item.Id, 'GET').done(function (data) {
            self.selectedQuery(data);
        });
    }

    self.saveEditedQuery = function (formElement) {
        var query = {
            Id:  self.editable.Id(),
            Url: self.editable.Url(),
            Body: self.editable.Body(),
            HttpCode: self.editable.HttpCode()
        };

        ajaxHelper(queryUri + query.Id, 'PUT', query).done(function () {
            getAllQueries();
            self.selectedQuery(null);
        });
    }

    self.addDefineApiCall = function (formElement) {
        var queryNoId = {
            Url: self.newQuery.Url(),
            Body: self.newQuery.Body(),
            HttpCode: self.newQuery.HttpCode()
        };

        ajaxHelper(defineApiCallUri, 'POST', queryNoId).done(function () {
            getAllQueries();
        });
    }

    self.getMoreStatistics = function (item) {
        ajaxHelper(listUri, 'POST', item).done(function (data) {
            self.moreStatistics(data);
        });
    }

    self.test = function (item) {
        ajaxHelper(item.Url, 'POST').done(function (data) {
            self.testUrl(data);
            getAllCalls();
        });
    }

    getAllQueries();
    getAllCalls();
};

ko.applyBindings(new ViewModel());