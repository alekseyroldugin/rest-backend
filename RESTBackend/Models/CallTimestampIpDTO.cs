﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTBackend.Models
{
    public class CallTimestampIpDTO
    {
        public DateTime Timestamp { get; set; }
        public string Ip { get; set; }
    }
}