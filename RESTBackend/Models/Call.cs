﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTBackend.Models
{
    public class Call
    {
        public int Id { get; set; }
        public int QueryId { get; set; }
        public DateTime Timestamp { get; set; }
        public string Ip { get; set; }
        public string Url { get; set; }
    }
}