﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTBackend.Models
{
    public class Query
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
        public int HttpCode { get; set; }
        //public DateTime Timestamp { get; set; }
        //public string Ip { get; set; }
    }
}