﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace RESTBackend.Models
{
    public class QueryContext : DbContext
    {
        public DbSet<Query> Queries { get; set; }
        public DbSet<Call> Calls { get; set; }
    }
}