﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTBackend.Models
{
    public class CallNoIdsDTO
    {
        public string Url { get; set; }
        public DateTime Timestamp { get; set; }
        public string Ip { get; set; }
    }
}