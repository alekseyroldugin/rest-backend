﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTBackend.Models
{
    public class QueryNoIdDTO
    {
        public string Url { get; set; }
        public string Body { get; set; }
        public int HttpCode { get; set; }
    }
}