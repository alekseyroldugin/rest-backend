﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTBackend.Models
{
    public class QueryUrlOnlyDTO
    {
        public string Url { get; set; }
    }
}