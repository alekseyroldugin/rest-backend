﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Routing;

namespace RESTBackend
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefineAPICallRoute",
                routeTemplate: "define-api-call",
                defaults: new { controller = "Queries", action = "DefineAPICall" },
                constraints: new { controller = "^Q.*" }
            );

            config.Routes.MapHttpRoute(
                name: "ListRoute",
                routeTemplate: "list",
                defaults: new { controller = "Queries", action = "List" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
            );

            config.Routes.MapHttpRoute(
                name: "GetCallsRoute",
                routeTemplate: "getcalls",
                defaults: new { controller = "Queries", action = "GetCalls" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );
            
            config.Routes.MapHttpRoute(
                name: "GetQueriesRoute",
                routeTemplate: "getqueries",
                defaults: new { controller = "Queries", action = "GetQueries" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );

            config.Routes.MapHttpRoute(
                name: "GetQueryRoute",
                routeTemplate: "getqueries/{id}",
                defaults: new { controller = "Queries", action = "GetQueries" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );

            config.Routes.MapHttpRoute(
                name: "PutQueryRoute",
                routeTemplate: "editquery/{id}",
                defaults: new { controller = "Queries", id = RouteParameter.Optional },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) }
            );
            
            config.Routes.MapHttpRoute(
                name: "UrlRoute",
                routeTemplate: "{*url}",
                defaults: new { controller = "Queries", action = "TestUrl", url = RouteParameter.Optional },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{parameter}",
                defaults: new { parameter = RouteParameter.Optional }
            );
        }
    }
}
