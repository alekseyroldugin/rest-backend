namespace RESTBackend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCallsTableAndRemoveColumsFromQueriesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Calls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QueryId = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Ip = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Queries", "Timestamp");
            DropColumn("dbo.Queries", "Ip");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Queries", "Ip", c => c.String());
            AddColumn("dbo.Queries", "Timestamp", c => c.DateTime(nullable: false));
            DropTable("dbo.Calls");
        }
    }
}
