namespace RESTBackend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUrlToCalls : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calls", "Url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calls", "Url");
        }
    }
}
