namespace RESTBackend.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using RESTBackend.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<RESTBackend.Models.QueryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RESTBackend.Models.QueryContext context)
        {
        }
    }
}
